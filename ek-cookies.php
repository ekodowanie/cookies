    <?php
    /*
      Plugin Name: Ciasteczka - ekodowanie.pl
      Version: 0.1
      Description: Powiadomienie o ciasteczkach.
      Author: ekodowanie.pl
      Author URI: https://ekodowanie.pl
     */

/*
  Load necessary scripts
*/
function efp_enqueued_scripts() {
  wp_enqueue_script( 'efp-script', plugin_dir_url( __FILE__ ) . '/js/jquery.cookies.js', null, '1.0', true );
}
add_action( 'wp_enqueue_scripts', 'efp_enqueued_scripts' );
/*
  Load necessary styles
*/
function efp_enqueued_styles() {
  wp_enqueue_style( 'efp-style', plugin_dir_url( __FILE__ ) . '/css/styles.css', null, '1.0' );
}
add_action( 'wp_enqueue_scripts', 'efp_enqueued_styles' );
/*
  Load HTML
*/
