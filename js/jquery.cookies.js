(function($, window, document, undefined) {
    
   console.log('loaded');
  
    function saveToLS(key, val) {

        if( !("localStorage" in window) ) return;

        window.localStorage.setItem(key, val);

    }

    function readFromLS(key) {

        if( !("localStorage" in window) ) return;

        return window.localStorage.getItem(key);

    }

    $.fn.cookieAlert = function(userOptions) {

        if( readFromLS("cookiesAccepted") === "1" ) {
            return this;
        }

        var options = $.extend({}, $.fn.cookieAlert.defaults, userOptions);

        var div = $("<div></div>", {
            "class": options.containerClass
        }).hide();

        var p = $("<p></p>", {
            "class": options.textClass,
            "text": options.message
        });

        var button = $("<div></div>", {
            "class": options.closeClass,
            "text": options.textButton
        });
      
        var terms = $("<span></span>", {
            "class": options.termsClass
        });
      
        var link = $("<a href='/polityka-prywatnosci'>Polityka prywatności</a>");

        button.on("click", function() {

            saveToLS("cookiesAccepted", 1);

            div.slideUp(options.animSpeed, function() {
                div.remove();
            });

        });
      
        $('.close-cookies').on("click", function() {

            saveToLS("cookiesAccepted", 1);

            div.slideUp(options.animSpeed, function() {
                div.remove();
            });

        });
        
        
        div.append(p);
    
        p.append(terms);
        terms.append(link);
      
      p.append(button);
       
        this.prepend(div);

        div.slideDown(options.animSpeed);

        return this;

    };

    $.fn.cookieAlert.defaults = {
        message: 'Używamy plików cookies w celu realizacji usług. Korzystając z tego serwisu wyrażasz zgodę na ich używanie. Jeżeli nie zmienisz ustawień przeglądarki, będą one zapisywane w pamięci Twojego urządzenia. ',
        animSpeed: 500,
        containerClass: "cookie",
        textClass: "cookie__text",
        closeClass: "cookie__close",
        termsClass: "cookie__terms",
        textButton: "Akceptuję",
    };
  
$(window).on("load", function() {

    $("body").cookieAlert({
        textClass: "cookie__text center-content"
    });

});

})(jQuery, window, document);



